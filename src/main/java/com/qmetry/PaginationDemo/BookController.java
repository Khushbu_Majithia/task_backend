package com.qmetry.PaginationDemo;

import java.util.List;

import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import javax.ws.rs.core.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses; 



@RestController
@RequestMapping("/todo/list")
@Api(value = "HelloWorld Resource", description = "shows hello world")
public class BookController {

    @Autowired
    BookRepository bookRepository;

    /*
    @GetMapping
    Page<Book> getBooks(
            @RequestParam Optional<Integer> page,
            @RequestParam Optional<String> sortBy
    ) {
        return bookRepository.findAll(
                PageRequest.of(
                        page.orElse(0),
                        5,
                        Sort.Direction.ASC, sortBy.orElse("id")
                )
        );
    }
    */
    
   
    
    /*
     * complete pagination and sorting
     */
    
    /*
	@PostMapping("/addbook")
    Page<Book> getBooks(@RequestBody PageDemo o) throws Exception
	{
    	if (o == null) {
			throw new Exception();
		}
		else {
			
		        return  (Page<Book>) bookRepository.findAll(
		        		PageRequest.of(
		                        o.getPageno(),
		                        10,
		                        o.getSortDirection().equals("ascending") ? Sort.by(o.getSortField()).ascending()
                                        : Sort.by(o.getSortField()).descending())
		         
		        		);
		        
		       
		}
	}
    
   */
 
@PostMapping("/gethere")
 public  String gethere(@RequestBody String o) throws JsonProcessingException
    {
	
	
	return bookRepository.applyfilter(o);
	
    	
}

@GetMapping("/findAllBooks")
@Consumes(MediaType.APPLICATION_JSON)
@ApiOperation(value = "Returns all book records")
@ApiResponses(
        value = {
             
                @ApiResponse(code = 200, message = "Successful return")
        }
)
public List<Book> findAllBooks()
{
	return bookRepository.findAll();
}

@GetMapping("/findBookById/{id}")
	public Optional<Book> findById(@RequestParam Long id)
	{
		return bookRepository.findById(id);
	
}




@PostMapping("/addbook")
@Produces(MediaType.APPLICATION_JSON)
public Book addBook(@RequestBody Book book)
{
	return bookRepository.save(book);
}

@DeleteMapping("/removeBook/{id}")
public void removeBook(@RequestParam Long id)
{
	bookRepository.deleteById(id);
}

//update book
@PutMapping("/updatebook")
public Book updateBook (@RequestBody Book request) 
{
	
    return bookRepository.save(request);
}

}


