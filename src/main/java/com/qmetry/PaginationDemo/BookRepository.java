package com.qmetry.PaginationDemo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface BookRepository extends JpaRepository<Book,Long>, BookRepositoryCustom {

	//Optional<Book> searchByTitle(String string);
	

	Page<Book> findByTitle(String string, Pageable pageable);

	



	
	}
