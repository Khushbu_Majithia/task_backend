package com.qmetry.PaginationDemo;

import java.util.List;


import javax.persistence.NamedQuery;
import javax.persistence.TypedQuery;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ObjectNode;

public interface BookRepositoryCustom {

	String applyfilter(String o) throws JsonProcessingException;
	
	//Page<Book> filtered(List<Book> listbooks, Pageable page);
	
	Book updateBook(Long bookId, Book modifybook);
	 
	Query totalelements();
	
}
