package com.qmetry.PaginationDemo;

import java.util.List;






import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Parameter;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.PrintWriter;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.util.Streamable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonKey;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import com.qmetry.PaginationDemo.Book;
import com.qmetry.PaginationDemo.PageDemo;
public class BookRepositoryCustomImpl implements BookRepositoryCustom {

	@PersistenceUnit
	 private EntityManagerFactory emf;
	 private BookRepository bookRepository;
	 
	@Autowired
	 private SessionFactory sessionFactory;
	
	
	
	
	@Override
	public String applyfilter(String o) throws JsonProcessingException {
		
		//json object user has passed
		JSONObject obj = new JSONObject(o);
		
		int n;
		int sortflag=0;
		int directionflag=0;
		//Pageable pageable;
		String dir;
		String fld;
		String s="";
		String qry ="",srch = null;
		String str="";	
		int start = 0;
		String sortquery="";
		String filterquery="";
		
		//if pageno is provided
		if(obj.has("pageno"))
		{
		n = obj.getInt("pageno");
		
		String st =Integer.toString(n);
		st+="0";
		start=Integer.parseInt(st);
		System.out.println("starting value="+start);
		}
		
		
		
		Object sortfield = null;
		Object sortdirection = null;
		Object field = null;
		Object query = null;
		
		//if user has provided sort parameters
		if(obj.has("sort"))
		{
			JSONArray ja = (JSONArray)obj.get("sort");
			
			
			for(int i=0;i<ja.length();i++)
			{
				JSONObject jo = new JSONObject();
				sortfield=ja.getJSONObject(i).get("field");
				
				//if user has provided sort direction
				if(ja.getJSONObject(i).has("direction"))
				{
					directionflag=1;
					sortdirection=ja.getJSONObject(i).get("direction");
				
				}		
			}
			
			//System.out.println(sortfield);
			//System.out.println(sortdirection);
			sortflag=1;
		}
		
		
	
		//if user has provided filter parameters
		if(obj.has("filter"))
		{
			JSONArray ja = (JSONArray)obj.get("filter");
			
			
			int l=ja.length();
			
			for(int i=0;i<l;i++)
			{
				JSONObject jo = new JSONObject();
				field=ja.getJSONObject(i).get("field");
				query=ja.getJSONObject(i).get("query");
				
				qry=query.toString();
				   
			    srch=field.toString();
			   
			    str+=srch;
			    str+=" LIKE ";
			    str+="'%";
			    str+=qry;
			    str+="%'";
			   
			    //in case of more than 1 filter provided
			   	if(l-i>1)
			   		str+=" and ";
			}
			
			
		   filterquery=" where "+str;
			//System.out.println(str);
		}
		
		

	    
	    if(sortflag==1)
	    {
	    	if(directionflag==1)
	    	{
	    	dir=sortdirection.toString();
	    	}
	    	
	    	else
	    	{
	    	dir="ascending";	//default direction->ascending
	    	}
	    	
	    	fld=sortfield.toString();
	    
	    sortquery=" order by "+sortfield+" "+sortdirection;
	    	 
	//pageable = PageRequest.of(n,10, dir.equals("ascending") ? Sort.by(fld).ascending()
	       // : Sort.by(fld).descending());
	    }
	    
	    else
	    {
	    	sortquery="";
	    }
	    
	    
	    EntityManager em = emf.createEntityManager();
	    
	    //@SuppressWarnings("unchecked")
	    //String stri="title='title6'";
	   
	    //List<Book> arr_cust =(List<Book>)em.createQuery("SELECT c FROM Book c where "+str).setMaxResults(1);
	    
	    
	    
	    Query q=em.createQuery("SELECT b FROM Book b"+filterquery+sortquery, Book.class).setFirstResult(start).setMaxResults(10);
	    List bookslist = q.getResultList();
	   
	    Long l;
	    
	    String hql="select count(*) from  books";
	    Session session = sessionFactory.openSession();
	    Query querytry= session.createNativeQuery(hql);
	    BigInteger count=(BigInteger)querytry.getSingleResult();
	    //System.out.println(count);
	    Long longcount = count.longValue();
	    
	   
	    /*Query checkQueryTotal = em.createNativeQuery(" SELECT FOUND_ROWS() from books");
	    Object total = checkQueryTotal.getSingleResult();
	    if (total instanceof BigInteger) {
	    BigInteger temp = (BigInteger) total;
	   System.out.println(temp);
	    } else {
	    System.out.println((long)total);
	    }*/
	    
	    ObjectMapper objectMapper = new ObjectMapper();
		
	
	 ObjectNode bookingDetails = objectMapper.createObjectNode();
	 
	 
	Long total_pages = longcount/10;
	if(longcount%10>0)
		total_pages+=1;
	
	 ArrayNode array = objectMapper.valueToTree(bookslist);
	 bookingDetails.put("page", start);
	 bookingDetails.put("per_page", 10);
	 bookingDetails.put("total_elements", longcount);
	 bookingDetails.put("total_pages", total_pages);
	 bookingDetails.putArray("data").addAll(array);
	
	
	 String createdPlainJsonObject = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(bookingDetails);
			// System.out.println("Created plain JSON Object is : \n"+ createdPlainJsonObject);
	 
	 return createdPlainJsonObject;
	 
	
	   /* 
	    return bookslist;
	    */

	 
	 
	
	    /*Iterator i = arr_cust.iterator();
	    Book cust = null;
	    while (i.hasNext()) {
		    cust = (Book) i.next();
		    System.out.println(cust.getId()+"");
		    System.out.println(cust.getTitle()+"");
	    }*/
	   
	  
	    //filtered(arr_cust,pageable);
	    
	
	}



	@Override
	public org.springframework.data.jpa.repository.Query totalelements() {
		 EntityManager em = emf.createEntityManager();
		    
		    //@SuppressWarnings("unchecked")
		    //String stri="title='title6'";
		   
		    //List<Book> arr_cust =(List<Book>)em.createQuery("SELECT c FROM Book c where "+str).setMaxResults(1);
		    
		    
		    
		    Query t=em.createNativeQuery("SELECT sql_calc_found_rows FROM Book",
		            Book.class);
		    
		    return (org.springframework.data.jpa.repository.Query) t;
	}
	    
	
	//update book
		 public Book updateBook(Long bookId,Book modifybook)
		 {
		 Optional<Book> book = bookRepository.findById(modifybook.getId());
	     if (!book.isPresent()) {
	         throw new EntityNotFoundException("Book Not Found");
	     }
	    
	     
	     Book updatebook = book.get();
	     updatebook.setTitle(modifybook.getTitle());
	     updatebook.setDescription(modifybook.getDescription());
	     return bookRepository.save(updatebook);
		}



		
		 
	}


