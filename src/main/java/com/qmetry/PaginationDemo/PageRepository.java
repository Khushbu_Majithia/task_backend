package com.qmetry.PaginationDemo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PageRepository extends JpaRepository<Book,Long> {

}
