package com.qmetry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaginationDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaginationDemoApplication.class, args);
		
		
		
	}

}
